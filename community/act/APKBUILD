# Contributor: Meng Zhuo <mengzhuo@iscas.ac.cn>
# Maintainer: Meng Zhuo <mengzhuo@iscas.ac.cn>
pkgname=act
pkgver=0.2.73
pkgrel=0
pkgdesc="Run your GitHub Actions locally"
url="https://github.com/nektos/act"
arch="all"
license="MIT"
depends="npm docker"
makedepends="go"
options="net" # go build requires
subpackages="$pkgname-doc"

source="$pkgname-$pkgver.tar.gz::https://github.com/nektos/act/archive/refs/tags/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir/go-tmp"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go-mod"}"

default_prepare() {
	mkdir -p $GOTMPDIR
	go fmt ./...
}

build() {
	go build -ldflags "-X main.version=$pkgver" -o act main.go
}

check() {
	./act --version
}

package() {
	install -Dm755 ./act -t "$pkgdir"/usr/bin/
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
4f6e3377b919ba8648fe64eb6337b55bcdc7de89879c8adff73d9df2e3980f457d5c7dc1bf1f77cb95082a0a18a3f48acc4888ba0eff8979ea4454a6a65d694e  act-0.2.73.tar.gz
"
